package main

import (
	"database/sql"
	"fmt"
	"github.com/codegangsta/martini"
	_ "github.com/lib/pq"
	"net/http"
)

func SetupDB() *sql.DB {
	db, err := sql.Open("postgres", "dbname=staging sslmode=disable")
	PanicIf(err)
	return db
}

func PanicIf(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	m := martini.Classic()
	m.Map(SetupDB())
	m.Get("/", func(rw http.ResponseWriter, r *http.Request, db *sql.DB) {
		search := "%" + r.URL.Query().Get("search") + "%"
		rows, err := db.Query(`SELECT title, description from posts_post WHERE title ILIKE $1 OR description ILIKE $1`, search)
		PanicIf(err)
		defer rows.Close()

		var title, description string
		for rows.Next() {
			err := rows.Scan(&title, &description)
			PanicIf(err)
			fmt.Fprintf(rw, "Title: %s\n Description: %s", title, description)
		}
	})

	m.Run()
}
