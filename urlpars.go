package main

import "fmt"
import "net/url"

func main() {
	s := "postgres://user:pass@host.com:5432/path?k=v#f"

	u, err := url.Parse(s)
	if err != nil {
		panic(err)
	}

	fmt.Println(u.Scheme)

	fmt.Println(u.User)

	fmt.Println(u.User.Username())

	fmt.Println(u.User)
	p, _ := u.User.Password()
	fmt.Println(p)
}
