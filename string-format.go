package main

import "fmt"
import "os"

type point struct {
	x, y int
}

func main() {
	p := point{1, 2}
	fmt.Printf("%v\n", p)

	fmt.Printf("%+v\n", p)
	fmt.Printf("%#v\n", p)
	fmt.Printf("%Tv\n", p)
	fmt.Printf("%t\n", true)

	fmt.Printf("%d\n", 123)
	fmt.Printf("%b\n", 12)
	fmt.Printf("%c\n", 33)

	fmt.Printf("%x\n", 234)
	fmt.Printf("%vf\n", 1212.22)
	fmt.Printf("|%-6s|%-6s|\n", "foo", "b")
	fmt.Fprintf(os.Stderr, "an %s\n", "error")
}
