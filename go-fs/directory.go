package fs

type Directory interface {
	Entry(name string) DirectoryEntry
	Entries() []DirectoryEntry
	AddDirectory(name string) (DirectoryEntry, error)
	AddFile(name string) (DirectoryEntry, error)
}

type DirectoryEntry interface {
	Name() string
	isDir() bool
	Dir() (Directory, error)
	File() (File, error)
}
