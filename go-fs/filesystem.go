package fs

// A Filesystem provide access to a tree hierachy of directories
// and files

type FileSystem interface {
	RootDir() (Directory, error)
}
