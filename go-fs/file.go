package fs

import "io"

type File interface {
	io.Reader
	io.Writer
}
