package main

import (
	"fmt"
	"net/http"

	"golang.org/x/net/html"
)

func main() {
	resp, err := http.Get("http://vinitkumar.me")
	fmt.Println("http transport error is:", err)
	fmt.Println("read error is:", err)
	doc := html.NewTokenizer(resp.Body)

	for {
		tt := doc.Next()
		switch {
		case tt == html.ErrorToken:
			return
		case tt == html.StartTagToken:
			t := doc.Token()

			isAnchor := t.Data == "a"
			if isAnchor {
				fmt.Println("Yay! a link found", t)
			}

		}
	}
	if err != nil {
		panic(err)
	}
}
