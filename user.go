package main

import (
	"database/sql"
	"github.com/codegangsta/martini"
	_ "github.com/lib/pq"
	"github.com/martini-contrib/render"
	"net/http"
)

type User struct {
	FirstName string
	Email     string
}

func setupDB() *sql.DB {
	db, err := sql.Open("postgres", "dbname=prod_today sslmode=disable")
	PanicIf(err)
	return db
}

func PanicIf(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	m := martini.Classic()
	m.Map(setupDB())
	m.Use(render.Renderer(render.Options{
		Layout: "layout",
	}))

	m.Get("/", func(ren render.Render, r *http.Request, db *sql.DB) {
		search := "%" + r.URL.Query().Get("search") + "%"
		rows, err := db.Query(`SELECT first_name, email from profiles_userprofile WHERE first_name ILIKE $1 OR email ILIKE $1`, search)
		PanicIf(err)
		defer rows.Close()

		users := []User{}
		for rows.Next() {
			user := User{}
			err := rows.Scan(&user.FirstName, &user.Email)
			PanicIf(err)
			users = append(users, user)
		}
		ren.HTML(200, "users", users)
	})

	m.Run()
}
