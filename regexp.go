package main

import "bytes"
import "fmt"
import "regexp"

func main() {
	match, _ := regexp.MatchString("p([a-z]+)ch", "peach")
	fmt.Println(match)

	r, _ := regexp.Compile("p([a-z]+)ch")

	fmt.Println(r.MatchString("peach"))
	fmt.Println(r.FindString("peach Punch"))
	fmt.Println(r.FindStringIndex("peach "))
	fmt.Println(r.FindStringSubmatch("peach punch"))

	in := []byte("a peach")
	out := r.ReplaceAllFunc(in, bytes.ToUpper)
	fmt.Println(string(out))
}
