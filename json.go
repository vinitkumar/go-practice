package main

import "encoding/json"
import "fmt"

type Response1 struct {
	Page   int
	Fruits []string
}
type Response2 struct {
	Page   int      `json:"page"`
	Fruits []string `json:"fruits"`
}

func main() {
	bolB, _ := json.Marshal(true)
	fmt.Println(bolB)

	intB, _ := json.Marshal(1)
	fmt.Println(intB)

	fltB, _ := json.Marshal(2.34)
	fmt.Println(fltB)
}
