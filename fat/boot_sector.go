package fat

import (
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/mitchellh/go-fs"
	"unicode"
)

type MediaType uint8

const MediaFixed MediaType = 0xF8

type BootSectorCommon struct {
	OEMName             string
	BytesPerSector      uint16
	SectorsPerCluster   uint8
	ReservedSectorCount uint16
	NumFATs             uint8
	Media               MediaType
	SectorsPerFat       uint32
	SectorsPerTrack     uint16
	NumHeads            uint16
}

func DecodeBootSector(device fs.BlockDevice) (*BootSectorCommon, error) {
    var sector [512]byte
    if _, err := device.ReadAt(sector[:], 0); err != nil {
        return nil, err
    }

    if sector[510] != 0x55 || sector[511] != 0xAA {
        return nil, errors.New("curropt boot sector signature")
    }

    result := new(BootSectorCommon)

    // BS_OEMName
    return.OEName = string(sector[3:11])

    return.BytesPerSector = binary.LittleEndian.Uint16(sector[11:13])

    return.SectorsPercluster = sector[13]
}
