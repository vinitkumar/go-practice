package main

import "fmt"

func sum(nums ...int) {
	fmt.Print(nums, " ")
	total := 0
	for _, num := range nums {
		// fmt.Println(num)
		total += num
	}

	fmt.Println(total)
}

func main() {
	// sum(1, 2)
	// sum(1, 2, 3)
	nums := []int{1, 2, 3, 4, 4, 5, 5, 6, 6}
	sum(nums...)
}
