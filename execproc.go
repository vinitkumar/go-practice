package main


import "syscall"
import "os"
import "os/exec"


func main() {
    binary, lookErr := exec.LookPath("ls")
    if lookErr != nil {
        Panic(lookErr)
    }

    args := []string{"ls", "-a", "-l", "-h"}

    env := os.Environ()
