package main

import (
	"database/sql"
	"github.com/codegangsta/martini"
	_ "github.com/lib/pq"
	"github.com/martini-contrib/render"
	"net/http"
)

type Post struct {
	Title       string
	Description string
}

func SetupDB() *sql.DB {
	db, err := sql.Open("postgres", "dbname=staging sslmode=disable")
	PanicIf(err)
	return db
}

func PanicIf(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	m := martini.Classic()
	m.Map(SetupDB())
	m.Use(render.Renderer(render.Options{
		Layout: "layout",
	}))

	m.Get("/", func(ren render.Render, r *http.Request, db *sql.DB) {
		search := "%" + r.URL.Query().Get("search") + "%"
		rows, err := db.Query(`SELECT title, description from posts_post WHERE title ILIKE $1 OR description ILIKE $1`, search)
		PanicIf(err)
		defer rows.Close()

		posts := []Post{}
		for rows.Next() {
			post := Post{}
			err := rows.Scan(&post.Title, &post.Description)
			PanicIf(err)
			posts = append(posts, post)
		}
		ren.HTML(200, "posts", posts)
	})

	m.Run()
}
